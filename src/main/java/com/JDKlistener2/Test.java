package com.JDKlistener2;

public class Test {
    public static void main(String[] args) {
        MoviceJDK moviceJDK = new MoviceJDK();

        XiaoMing xiaoMing = new XiaoMing();
        moviceJDK.addObserver(new XiaoGang());
        moviceJDK.addObserver(xiaoMing);
        moviceJDK.deleteObserver(xiaoMing);
        moviceJDK.Movice();

    }
}
