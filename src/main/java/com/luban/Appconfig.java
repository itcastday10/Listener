package com.luban;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan("com.luban")
@Configuration
public class Appconfig {
}
