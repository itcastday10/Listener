package com.luban;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {


    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac =new AnnotationConfigApplicationContext();
        ac.register(Appconfig.class);
        ac.refresh();
        ac.getBean(UserDao.class).query();
    }
}
